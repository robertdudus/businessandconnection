import "robertdudus-business";
import "robertdudus-connection";
import { Component, ComponentInterface } from "@stencil/core";

@Component({
  tag: "ixt-business-and-connection",
  styleUrl: "business-and-connection.css",
  shadow: true
})
export class BusinessAndConnection implements ComponentInterface {
  render() {
    return (
      <div>
        <ixt-business />
        <ixt-connection />
      </div>
    );
  }
}
